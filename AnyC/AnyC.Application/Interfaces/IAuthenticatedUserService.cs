﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnyC.Application.Interfaces
{
    public interface IAuthenticatedUserService
    {
        string UserId { get; }
    }
}
