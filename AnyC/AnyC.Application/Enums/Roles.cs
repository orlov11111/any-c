﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnyC.Application.Enums
{
    public enum Roles
    {
        SuperAdmin,
        Admin,
        Moderator,
        Basic
    }
}
